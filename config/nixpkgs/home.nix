{ config, pkgs, ... }:

{
  # Home Manager needs a bit of information about you and the
  # paths it should manage.
  home.username = "frowst";
  home.homeDirectory = "/home/frowst";

  # This value determines the Home Manager release that your
  # configuration is compatible with. This helps avoid breakage
  # when a new Home Manager release introduces backwards
  # incompatible changes.
  #
  # You can update Home Manager without changing this value. See
  # the Home Manager release notes for a list of state version
  # changes in each release.
  home.stateVersion = "21.11";

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;
  programs.bash = {
    enable = true;
    profileExtra = ''
      [ -z $DISPLAY ] && [ $(tty) = /dev/tty1 ] && sway
    '';
    bashrcExtra = ''
      export PS1="\W $ "
      export EDITOR="vim"
      export BROWSER="firefox"
      export TERM="foot"
      export MOZ_ENABLE_WAYLAND=1

      alias musicdl='yt-dlp --add-metadata -f bestaudio' # I want music
      alias rm='trash' # Safe file removal (restorable deletion)

    '';
  };
  
  # Vim configuration
  programs.vim = {
    enable = true;
    extraConfig = ''
    	syntax on
    	set tabstop=2
	    set shiftwidth=2
	    set expandtab
	    set ai
	    set hlsearch
	    set ruler
	    highlight Comment ctermfg=green

      map <F8> :w <CR> :!gcc % -o %< && ./%< <CR>
    '';
  };

  programs.starship = {
    enable = true;
    enableZshIntegration = true;
    settings = {
      # ~/.config/starship.toml

      # Use custom format
      format = '' 
      [](bold gray)  $directory$git_status
      [ﳣ](bold gray) '';

      # Wait 10 milliseconds for starship to check files under the current directory.
      scan_timeout = 10;

      # Disable the blank line at the start of the prompt
      add_newline = false;
    };
  };

  # Git configuration
	programs.git = {
    enable = true;
    userName  = "Anonymous";
    userEmail = "alizainxweb@protonmail.com";
  };
}
