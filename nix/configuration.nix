#   \\  \\ //      NixOS "minimal" configuration
#  ==\\__\\/ //    
#    //   \\//     "I like things to be minimal. "
# ==//     //==    - Sun Tzu
#  //\\___//       
# // /\\  \\==
#   // \\  \\


{ config, pkgs, ... }:

{
  imports =
    [
      ./hardware-configuration.nix
    ];

  # Allow unfree firmware
  nixpkgs.config.allowUnfree = true;

  # Bootloader configuration
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # Load radeon modules
  boot.initrd.kernelModules = [ "radeon" ];

  # Networking and time setup
  networking.hostName = "smoll";
  time.timeZone = "Asia/Karachi";
  networking.useDHCP = false;
  networking.interfaces.enp0s25.useDHCP = true;

  # Locale and console font
  i18n.defaultLocale = "en_US.UTF-8";
  console = {
    font = "Lat2-Terminus16";
    keyMap = "us";
  };

  # Sound shenanigans
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # CPU microcode and firmware
  hardware.cpu.intel.updateMicrocode = true;
  hardware.enableAllFirmware = true;
  hardware.enableRedistributableFirmware = true;  
  
  # GPU
  hardware.opengl.enable = true;
  hardware.opengl.driSupport = true;

  # Sway setup
  programs.sway = {
    enable = true;
    wrapperFeatures.gtk = true;
    extraPackages = with pkgs; [
      wl-clipboard
      waybar
      grim
      slurp
      dunst 
      foot
      rofi
      rofimoji
    ];
  };

  # User setup
  users.users.frowst = {
    isNormalUser = true;
    extraGroups = [ "wheel" "tty" "audio" "video" "input" ]; 
  };

  # Installed system packages
  environment.systemPackages = with pkgs; [
    vim trash-cli yt-dlp git # CLI shit
    mpv imv firefox papirus-icon-theme keepassxc gnome.adwaita-icon-theme # GUI shit
    gcc # compiler (duh)
  ];

  # You know the game and so do I.
  system.stateVersion = "21.11";

}

